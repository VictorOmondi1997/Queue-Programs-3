#include <iostream>
#include <conio.h>
#include <stdlib.h>
#define SIZE 5

using namespace std;

int q[SIZE],front=0,rear=0;

int main()
{
    int ch;
    //clrscr
    void enqueue();
    void dequeue();
    void display();
    while(1){
        cout<< "\n1.Add Element";
        cout<< "\n2.Remove Element";
        cout<< "\n3.Display";
        cout<< "\n4.Exit";
        cout<< "\nEnter Your Choice: ";
        cin>> ch;
        //clrscr();
        switch(ch){
        case 1:
            enqueue();
            break;
        case 2:
            dequeue();
            break;
        case 3:
            display();
            break;
        case 4:
            exit(0);
        default:
            cout<< "\nInvalid Choice";
        }
    }
}
void enqueue(){
    int no;
    if(rear==SIZE && front==0)
        cout<< "Queue is Full";
    else{
        cout<< "\nEnter the Number: ";
        cin>> no;
        q[rear]=no;
    }
    rear++;
}
void dequeue(){
    int no,i;
    if(front==rear)
        cout<< "\nQueue is Empty";
    else{
        no=q[front];
        front++;
        cout<< "\n"<< no<< "-removed from the Queue\n";
    }
}
void display(){
    int i,temp=front;
    if(front==rear)
        cout<< "The Queue is Empty";
    else{
        cout<< "\nElement in The Queue: ";
        for(i=temp;i<rear;i++){
            cout<< q[i]<< " ";
        }
    }
}
